<%-- 
    Document   : index
    Created on : 2018.11.23., 11:20:54
    Author     : Vendel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            body{
                font-family: Arial;
                font-size: 14px;
            }
            .container{
                display: block;
                margin: 0 auto;
                width: 300px;
            }
        </style>
    </head>
    <body>
        <form class="container" action="Servlet" method="GET">
            <table>
                <tr>
                    <td>Your name</td>
                    <td><input type="text" name="name" placeholder="Your full name"/></td>
                </tr>
                <tr>
                    <td>Car name</td>
                    <td><input type="text" name="car" placeholder="Car Brand and Model"/></td>
                </tr>
                <tr>
                    <td>Base price</td>
                    <td><input type="number" name="price"/></td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" value="Get my prices"/></td>
                </tr>
            </table>
        </form>
    </body>
</html>
