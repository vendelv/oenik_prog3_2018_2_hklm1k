﻿// <copyright file="BrandRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System;
    using System.Linq;
    using CarShop.Data;

    /// <summary>
    /// CRUD operation for Brands
    /// </summary>
    public class BrandRepository : IRepository<Brands>, IDisposable
    {
        private DatabaseEntities db = new DatabaseEntities();

        /// <summary>
        /// Gets select all instance from Database Entity
        /// </summary>
        /// <returns>A list of Instances</returns>
        public IQueryable<Brands> SelectAll => this.db.Brands;

        /// <summary>
        /// Create instance to Database Entity
        /// </summary>
        /// <param name="data">Instance</param>
        /// <returns>Returns true if the add was successful</returns>
        public bool Create(Brands data)
        {
            this.db.Brands.Add(data);
            return this.db.SaveChanges() > 0 ? true : false;
        }

        /// <summary>
        /// Delete instance from Database Entity by Instance
        /// </summary>
        /// <param name="data">Instance</param>
        /// <returns>Returns true if the delete was successful</returns>
        public bool Delete(Brands data)
        {
            foreach (modelExtraConnect m in this.db.modelExtraConnect.Where(x => x.Models.Brands.Id == data.Id))
            {
                this.db.modelExtraConnect.Remove(m);
            }

            foreach (Models m in this.db.Models.Where(x => x.Brands.Id == data.Id))
            {
                this.db.Models.Remove(m);
            }

            this.db.Brands.Remove(data);
            return this.db.SaveChanges() > 0 ? true : false;
        }

        /// <summary>
        /// Delete instance from Database Entity by ID
        /// </summary>
        /// <param name="id">Unique id</param>
        /// <returns>Returns true if the delete was successful</returns>
        public bool Delete(int id)
        {
            foreach (modelExtraConnect m in this.db.modelExtraConnect.Where(x => x.Models.brand_id == id))
            {
                this.db.modelExtraConnect.Remove(m);
            }

            foreach (Models m in this.db.Models.Where(x => x.brand_id == id))
            {
                this.db.Models.Remove(m);
            }

            this.db.Brands.Remove(this.db.Brands.Where(x => x.Id == id).SingleOrDefault());
            return this.db.SaveChanges() > 0 ? true : false;
        }

        /// <summary>
        /// Update instance to Database Entity
        /// </summary>
        /// <param name="data">Updated instance</param>
        /// <param name="field">Selected field</param>
        /// <param name="value">New value</param>
        /// <returns>Returns true if the update was successful</returns>
        public bool Update(Brands data, string field, string value)
        {
            var myitem = this.db.Brands.Where(x => x.Id == data.Id).SingleOrDefault();
            switch (field.ToLower())
            {
                case "name":
                    myitem.name = value;
                    break;
                case "country":
                    myitem.country = value;
                    break;
                case "url":
                    myitem.url = value;
                    break;
                case "founded":
                    myitem.founded = int.Parse(value);
                    break;
                case "income":
                    myitem.income = int.Parse(value);
                    break;
                default:
                    throw new ArgumentException("Invalid field!");
            }

            return this.db.SaveChanges() > 0 ? true : false;
        }

        /// <summary>
        /// Dispose database
        /// Source: https://docs.microsoft.com/en-us/visualstudio/code-quality/ca1063-implement-idisposable-correctly?view=vs-2017
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// The bulk of the clean-up code is implemented in Dispose(bool)
        /// </summary>
        /// <param name="disposing">disposing</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (this.db != null)
                {
                    this.db.Dispose();
                    this.db = null;
                }
            }
        }
    }
}
