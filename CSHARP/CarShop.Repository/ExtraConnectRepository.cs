﻿// <copyright file="ExtraConnectRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System;
    using System.Linq;
    using CarShop.Data;

    /// <summary>
    /// CRUD operation for Models
    /// </summary>
    public class ExtraConnectRepository : IRepository<modelExtraConnect>, IDisposable
    {
        private DatabaseEntities db = new DatabaseEntities();

        /// <summary>
        /// Gets select all instance from Database Entity
        /// </summary>
        /// <returns>A list of Instances</returns>
        public IQueryable<modelExtraConnect> SelectAll => this.db.modelExtraConnect;

        /// <summary>
        /// Create instance to Database Entity
        /// </summary>
        /// <param name="data">Instance</param>
        /// <returns>Returns true if the add was successful</returns>
        public bool Create(modelExtraConnect data)
        {
            if (data.Models == null)
            {
                data.Models = this.db.Models.Where(x => x.Id == data.model_id).SingleOrDefault();
            }

            if (data.Extras == null)
            {
                data.Extras = this.db.Extras.Where(x => x.Id == data.extra_id).SingleOrDefault();
            }

            this.db.modelExtraConnect.Add(data);
            return this.db.SaveChanges() > 0 ? true : false;
        }

        /// <summary>
        /// Delete instance from Database Entity by Instance
        /// </summary>
        /// <param name="data">Instance</param>
        /// <returns>Returns true if the delete was successful</returns>
        public bool Delete(modelExtraConnect data)
        {
            this.db.modelExtraConnect.Remove(data);
            return this.db.SaveChanges() > 0 ? true : false;
        }

        /// <summary>
        /// Delete instance from Database Entity by ID
        /// </summary>
        /// <param name="id">Unique id</param>
        /// <returns>Returns true if the delete was successful</returns>
        public bool Delete(int id)
        {
            this.db.modelExtraConnect.Remove(this.db.modelExtraConnect.Where(x => x.Id == id).SingleOrDefault());
            return this.db.SaveChanges() > 0 ? true : false;
        }

        /// <summary>
        /// Update instance to Database Entity
        /// </summary>
        /// <param name="data">Updated instance</param>
        /// <param name="field">Selected field</param>
        /// <param name="value">New value</param>
        /// <returns>Returns true if the update was successful</returns>
        public bool Update(modelExtraConnect data, string field, string value)
        {
            var myitem = this.db.modelExtraConnect.Where(x => x.Id == data.Id).SingleOrDefault();
            switch (field.ToLower())
            {
                case "model_id":
                    myitem.model_id = int.Parse(value);
                    myitem.Models = this.db.Models.Where(x => x.Id == myitem.model_id).SingleOrDefault();
                    break;
                case "extra_id":
                    myitem.extra_id = int.Parse(value);
                    myitem.Extras = this.db.Extras.Where(x => x.Id == myitem.extra_id).SingleOrDefault();
                    break;
                default:
                    throw new ArgumentException("Invalid field!");
            }

            return this.db.SaveChanges() > 0 ? true : false;
        }

        /// <summary>
        /// Dispose database
        /// Source: https://docs.microsoft.com/en-us/visualstudio/code-quality/ca1063-implement-idisposable-correctly?view=vs-2017
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// The bulk of the clean-up code is implemented in Dispose(bool)
        /// </summary>
        /// <param name="disposing">disposing</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (this.db != null)
                {
                    this.db.Dispose();
                    this.db = null;
                }
            }
        }
    }
}
