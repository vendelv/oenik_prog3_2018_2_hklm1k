﻿// <copyright file="ModelRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System;
    using System.Linq;
    using CarShop.Data;

    /// <summary>
    /// CRUD operation for Models
    /// </summary>
    public class ModelRepository : IRepository<Models>, IDisposable
    {
        private DatabaseEntities db = new DatabaseEntities();

        /// <summary>
        /// Gets select all instance from Database Entity
        /// </summary>
        /// <returns>A list of Instances</returns>
        public IQueryable<Models> SelectAll => this.db.Models;

        /// <summary>
        /// Create instance to Database Entity
        /// </summary>
        /// <param name="data">Instance</param>
        /// <returns>Returns true if the add was successful</returns>
        public bool Create(Models data)
        {
            this.db.Models.Add(data);
            return this.db.SaveChanges() > 0 ? true : false;
        }

        /// <summary>
        /// Delete instance from Database Entity by Instance
        /// </summary>
        /// <param name="data">Instance</param>
        /// <returns>Returns true if the delete was successful</returns>
        public bool Delete(Models data)
        {
            foreach (modelExtraConnect m in this.db.modelExtraConnect.Where(x => x.model_id == data.Id))
            {
                this.db.modelExtraConnect.Remove(m);
            }

            this.db.Models.Remove(data);
            return this.db.SaveChanges() > 0 ? true : false;
        }

        /// <summary>
        /// Delete instance from Database Entity by ID
        /// </summary>
        /// <param name="id">Unique id</param>
        /// <returns>Returns true if the delete was successful</returns>
        public bool Delete(int id)
        {
            foreach (modelExtraConnect m in this.db.modelExtraConnect.Where(x => x.model_id == id))
            {
                this.db.modelExtraConnect.Remove(m);
            }

            this.db.Models.Remove(this.db.Models.Where(x => x.Id == id).SingleOrDefault());
            return this.db.SaveChanges() > 0 ? true : false;
        }

        /// <summary>
        /// Update instance to Database Entity
        /// </summary>
        /// <param name="data">Updated instance</param>
        /// <param name="field">Selected field</param>
        /// <param name="value">New value</param>
        /// <returns>Returns true if the update was successful</returns>
        public bool Update(Models data, string field, string value)
        {
            var myitem = this.db.Models.Where(x => x.Id == data.Id).SingleOrDefault();
            switch (field.ToLower())
            {
                case "brand_id":
                    myitem.brand_id = int.Parse(value);
                    break;
                case "name":
                    myitem.name = value;
                    break;
                case "releasedate":
                    myitem.releasedate = DateTime.Parse(value);
                    break;
                case "engine_volume":
                    myitem.engine_volume = int.Parse(value);
                    break;
                case "horsepower":
                    myitem.horsepower = int.Parse(value);
                    break;
                case "price":
                    myitem.price = int.Parse(value);
                    break;
                default:
                    throw new ArgumentException("Invalid field!");
            }

            return this.db.SaveChanges() > 0 ? true : false;
        }

        /// <summary>
        /// Dispose database
        /// Source: https://docs.microsoft.com/en-us/visualstudio/code-quality/ca1063-implement-idisposable-correctly?view=vs-2017
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// The bulk of the clean-up code is implemented in Dispose(bool)
        /// </summary>
        /// <param name="disposing">disposing</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (this.db != null)
                {
                    this.db.Dispose();
                    this.db = null;
                }
            }
        }
    }
}
