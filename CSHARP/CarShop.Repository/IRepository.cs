﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System.Linq;
    using CarShop.Data;

    /// <summary>
    /// CRUD operation interface
    /// </summary>
    /// <typeparam name="T">T is the current carshop model</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// Gets select all instance from Database Entity
        /// </summary>
        /// <returns>A list of Instances</returns>
        IQueryable<T> SelectAll { get; }

        /// <summary>
        /// Create instance to Database Entity
        /// </summary>
        /// <param name="data">Instance</param>
        /// <returns>Returns true if the add was successful</returns>
        bool Create(T data);

        /// <summary>
        /// Delete instance from Database Entity by Instance
        /// </summary>
        /// <param name="data">Instance</param>
        /// <returns>Returns true if the delete was successful</returns>
        bool Delete(T data);

        /// <summary>
        /// Delete instance from Database Entity by ID
        /// </summary>
        /// <param name="id">Unique id</param>
        /// <returns>Returns true if the delete was successful</returns>
        bool Delete(int id);

        /// <summary>
        /// Update instance to Database Entity
        /// </summary>
        /// <param name="data">Updated instance</param>
        /// <param name="field">Selected field</param>
        /// <param name="value">New value</param>
        /// <returns>Returns true if the update was successful</returns>
        bool Update(T data, string field, string value);

        /// <summary>
        /// Dispose object
        /// </summary>
        void Dispose();
    }
}
