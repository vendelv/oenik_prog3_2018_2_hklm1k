﻿// <copyright file="MyApp.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Core;
    using System.Linq;
    using System.Net;
    using System.Xml.Linq;
    using CarShop.Data;
    using CarShop.Logic;

    /// <summary>
    /// Main program class
    /// </summary>
    public static class MyApp
    {
        private static int menuBrand = 0;
        private static int menuModel = 0;

        /// <summary>
        /// The main entering point of the program
        /// </summary>
        public static void Main()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Welcome to the CarShop app!");
                Console.WriteLine("---------------------------");
                if (menuBrand == 0)
                {
                    Console.WriteLine("BRANDS:");
                    BrandLogic brands = new BrandLogic();
                    brands.List();
                    Console.WriteLine("---------------------------");
                    Console.WriteLine("Commands:\nselect\tSelect a brand\nadd\tCreate a new brand\nremove\tDelete a brand");
                    Console.WriteLine("---------------------------");
                    Console.Write("\nCarShop> ");
                    string answear = Console.ReadLine();
                    switch (answear)
                    {
                        case "back":
                            WriteAndClear("You're already in the Home Page!", true);
                            continue;
                        case "add":
                            Console.Clear();
                            brands.CreateForm();
                            Console.ReadKey();
                        break;
                        case "update":
                            Console.Write("Brand ID: ");
                            int id = int.Parse(Console.ReadLine());
                            Console.Write("Which row?: ");
                            string field = Console.ReadLine();
                            Console.Write("New value: ");
                            string value = Console.ReadLine();
                            try
                            {
                                brands.Update(id, field, value);
                            }
                            catch (ArgumentException e)
                            {
                                Console.WriteLine(e.Message);
                            }

                            Console.ReadKey();
                        break;
                        case "remove":
                            Console.Write("Brand ID: ");
                            brands.Delete(int.Parse(Console.ReadLine()));
                            Console.ReadKey();
                        break;
                        case "select":
                            Console.Write("Brand ID: ");
                            answear = Console.ReadLine();
                            List<int> ids = brands.GetId("id", answear).ToList();
                            if (ids.Count == 0)
                            {
                                WriteAndClear("Not existing brand.", true);
                            }
                            else if (ids.Count > 1)
                            {
                                WriteAndClear("More than one result!", true);
                            }
                            else
                            {
                                menuBrand = ids[0];
                            }

                        break;
                        default:
                            WriteAndClear("Unknown command!", true);
                        break;
                    }
                }
                else if (menuModel == 0)
                {
                    Console.WriteLine("Brand MODELS:");
                    ModelLogic models = new ModelLogic();
                    models.List(menuBrand);
                    Console.WriteLine("---------------------------");
                    Console.WriteLine("Commands:\noffer\tGet a offer from the Java endpoint\nselect\tSelect a model\nadd\tCreate a new model\nremove\tDelete a model\nback\tGo back to Brands");
                    Console.WriteLine("---------------------------");
                    Console.Write("\nCarShop> ");
                    string answear = Console.ReadLine();
                    switch (answear)
                    {
                        case "back":
                            menuBrand = 0;
                            break;
                        case "add":
                            Console.Clear();
                            models.CreateForm(menuBrand);
                            Console.ReadKey();
                            break;
                        case "update":
                            Console.Write("Model ID: ");
                            int id = int.Parse(Console.ReadLine());
                            Console.Write("Which row?: ");
                            string field = Console.ReadLine();
                            Console.Write("New value: ");
                            string value = Console.ReadLine();
                            try
                            {
                                models.Update(id, field, value);
                            }
                            catch (ArgumentException e)
                            {
                                Console.WriteLine(e.Message);
                            }

                            Console.ReadKey();
                            break;
                        case "remove":
                            Console.Write("Model ID: ");
                            models.Delete(int.Parse(Console.ReadLine()));
                            Console.ReadKey();
                            break;
                        case "offer":
                            Console.Write("Model ID: ");
                            Models a = models.GetById(int.Parse(Console.ReadLine()));
                            Console.Write("Your name: ");
                            string name = Console.ReadLine();
                            if (a != null)
                            {
                                try
                                {
                                    XDocument xd = XDocument.Load("http://127.0.0.1:8080/JAVA/Servlet?name=" + name + "&car=" + a.Brands.name + " " + a.name + "&price=" + a.price);
                                    Console.WriteLine("YOUR Offers:");
                                    Console.WriteLine("Name\tCar\t\tPrice");
                                    foreach (var item in xd.Descendants("offer"))
                                    {
                                        Console.WriteLine($"{item.Element("name").Value}\t{item.Element("car").Value}\t{item.Element("price").Value}");
                                    }
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e.Data + ": " + e.Message);
                                }
                            }

                            Console.ReadKey();
                            break;
                        case "select":
                            Console.Write("Model ID: ");
                            answear = Console.ReadLine();
                            List<int> ids = models.GetId("id", answear).ToList();
                            if (ids.Count == 0)
                            {
                                WriteAndClear("Not existing model.", true);
                            }
                            else if (ids.Count > 1)
                            {
                                WriteAndClear("More than one result!", true);
                            }
                            else
                            {
                                menuModel = ids[0];
                            }

                            break;
                        default:
                            WriteAndClear("Unknown command!", true);
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Model EXTRAS:");
                    ExtrasLogic extras = new ExtrasLogic();
                    ExtraConnectLogic extraconnect = new ExtraConnectLogic();
                    Console.WriteLine("Extras in this modell:");
                    extras.List(menuModel);
                    Console.WriteLine("---------------------------");
                    Console.WriteLine("Commands:\nconnect\tAdd a Extra to this Model\ndisconnect\tRemove a Extra to this Model\nadd\tCreate a new extra\nremove\tDelete a extra\nback\tGo back to Models");
                    Console.WriteLine("---------------------------");
                    Console.WriteLine("All extra:");
                    extras.List();
                    Console.WriteLine("---------------------------");
                    Console.Write("\nCarShop> ");
                    string answear = Console.ReadLine();
                    switch (answear)
                    {
                        case "back":
                            menuModel = 0;
                            break;
                        case "add":
                            Console.Clear();
                            extras.CreateForm(menuModel);
                            Console.ReadKey();
                            break;
                        case "update":
                            Console.Write("Extra ID: ");
                            int id = int.Parse(Console.ReadLine());
                            Console.Write("Which row?: ");
                            string field = Console.ReadLine();
                            Console.Write("New value: ");
                            string value = Console.ReadLine();
                            try
                            {
                                extras.Update(id, field, value);
                            }
                            catch (ArgumentException e)
                            {
                                Console.WriteLine(e.Message);
                            }

                            Console.ReadKey();
                            break;
                        case "remove":
                            Console.Write("Extra ID: ");
                            extras.Delete(int.Parse(Console.ReadLine()));
                            Console.ReadKey();
                            break;
                        case "connect":
                            extraconnect.CreateForm(menuModel);
                            Console.ReadKey();
                            break;
                        case "disconnect":
                            Console.Write("Extra ID: ");
                            extraconnect.Delete(int.Parse(Console.ReadLine()));
                            Console.ReadKey();
                            break;
                        default:
                            WriteAndClear("Unknown command!", true);
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Clear the console and write some text
        /// </summary>
        /// <param name="text">The text</param>
        /// <param name="wait">Wait for keypress</param>
        private static void WriteAndClear(string text, bool wait = false)
        {
            Console.Clear();
            Console.WriteLine(text + "\n");
            if (wait == true)
            {
                Console.ReadKey();
            }
        }
    }
}
