var class_car_shop_1_1_logic_1_1_extras_logic =
[
    [ "ExtrasLogic", "class_car_shop_1_1_logic_1_1_extras_logic.html#aa2c8e30131aaecade078ada4d9ff85c8", null ],
    [ "Create", "class_car_shop_1_1_logic_1_1_extras_logic.html#ae073744ad871cfc0dbc0a8d325630b2c", null ],
    [ "CreateForm", "class_car_shop_1_1_logic_1_1_extras_logic.html#a8eef72cf53a14357866f9f14ce703b00", null ],
    [ "Delete", "class_car_shop_1_1_logic_1_1_extras_logic.html#ab69ce986a542e7f16a61f12534a93958", null ],
    [ "Dispose", "class_car_shop_1_1_logic_1_1_extras_logic.html#ae71eb254bfa2b8ca85421d491f82ad3c", null ],
    [ "Dispose", "class_car_shop_1_1_logic_1_1_extras_logic.html#a044dabbcb09f1eef2b1628d988854784", null ],
    [ "GetById", "class_car_shop_1_1_logic_1_1_extras_logic.html#a1c360d7c3e2bc9e7a1c09a7b089bc7f2", null ],
    [ "GetId", "class_car_shop_1_1_logic_1_1_extras_logic.html#a408bc3b3b324ab85d9199a9d16ffe003", null ],
    [ "GetModelExtras", "class_car_shop_1_1_logic_1_1_extras_logic.html#a53a54ab1a8784e4294e965d62c1db2ac", null ],
    [ "GetModelExtras", "class_car_shop_1_1_logic_1_1_extras_logic.html#ae1c7b25943b4277ca3a022d35d146937", null ],
    [ "List", "class_car_shop_1_1_logic_1_1_extras_logic.html#a6d4c3bd6cd89a4b2a2392b32ac24e2c4", null ],
    [ "Update", "class_car_shop_1_1_logic_1_1_extras_logic.html#ae37caf0899373ca3496a5f8d79573c8c", null ]
];