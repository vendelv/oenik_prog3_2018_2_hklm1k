var class_car_shop_1_1_logic_1_1_brand_logic =
[
    [ "BrandLogic", "class_car_shop_1_1_logic_1_1_brand_logic.html#a22509f6f2bb94e68644e43262459d1f7", null ],
    [ "Create", "class_car_shop_1_1_logic_1_1_brand_logic.html#a187a2dedce1e4abb4d41c578ff9d0f57", null ],
    [ "CreateForm", "class_car_shop_1_1_logic_1_1_brand_logic.html#add9e08fd3f842c61078addd64a4c7713", null ],
    [ "Delete", "class_car_shop_1_1_logic_1_1_brand_logic.html#aed9d0a854e395db9e5295b726328ae63", null ],
    [ "Dispose", "class_car_shop_1_1_logic_1_1_brand_logic.html#ad0ca0197c09f97c406bad1f96a1f3f10", null ],
    [ "Dispose", "class_car_shop_1_1_logic_1_1_brand_logic.html#a095bd08c6e3be31f40bcfd60539a5381", null ],
    [ "GetById", "class_car_shop_1_1_logic_1_1_brand_logic.html#a41ae9f5fa446686d314850a28d9c535f", null ],
    [ "GetId", "class_car_shop_1_1_logic_1_1_brand_logic.html#a6b14ba5db1f206484394b87b4a225e01", null ],
    [ "GetModels", "class_car_shop_1_1_logic_1_1_brand_logic.html#a944c7a7facac8fe1497724e85bd09cdc", null ],
    [ "List", "class_car_shop_1_1_logic_1_1_brand_logic.html#a4e5b579623fd10782e14b45afe53c0a9", null ],
    [ "Update", "class_car_shop_1_1_logic_1_1_brand_logic.html#a1f30aade6a605729c534b9682d435678", null ]
];