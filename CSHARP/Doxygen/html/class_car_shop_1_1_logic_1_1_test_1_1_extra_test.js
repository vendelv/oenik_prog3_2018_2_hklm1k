var class_car_shop_1_1_logic_1_1_test_1_1_extra_test =
[
    [ "CanAddExtra", "class_car_shop_1_1_logic_1_1_test_1_1_extra_test.html#a7ca1fe50721683a817c34ddbdc3e0f53", null ],
    [ "CanGetExtra", "class_car_shop_1_1_logic_1_1_test_1_1_extra_test.html#a4077fd3e4e7c451b18b7865614fbcde0", null ],
    [ "CanRemoveExtra", "class_car_shop_1_1_logic_1_1_test_1_1_extra_test.html#a79b0389818756b84fd62514dd9e00177", null ],
    [ "CanUpdateExtra", "class_car_shop_1_1_logic_1_1_test_1_1_extra_test.html#ab5798fa197723cb1b95fbc69a9056f22", null ],
    [ "Dispose", "class_car_shop_1_1_logic_1_1_test_1_1_extra_test.html#a32c6591870bcf2d87a2504da37a3e5af", null ],
    [ "Dispose", "class_car_shop_1_1_logic_1_1_test_1_1_extra_test.html#a84d8fb9b5b2195c84a2ea90201ce530d", null ],
    [ "Setup", "class_car_shop_1_1_logic_1_1_test_1_1_extra_test.html#a3b92d833ea454f7aa0cef4b9a21588c2", null ]
];