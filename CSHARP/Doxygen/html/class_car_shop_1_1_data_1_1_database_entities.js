var class_car_shop_1_1_data_1_1_database_entities =
[
    [ "DatabaseEntities", "class_car_shop_1_1_data_1_1_database_entities.html#ad9f6fcd62419012bb952d998d93eadc4", null ],
    [ "OnModelCreating", "class_car_shop_1_1_data_1_1_database_entities.html#a9383e95b9b53b1c4688a20d7af17a8c0", null ],
    [ "Brands", "class_car_shop_1_1_data_1_1_database_entities.html#a8fdc895515efe9dec50292a5ab6e30fb", null ],
    [ "Extras", "class_car_shop_1_1_data_1_1_database_entities.html#a2263d4ef1e4656f3b7e60fe33956ad65", null ],
    [ "modelExtraConnect", "class_car_shop_1_1_data_1_1_database_entities.html#afa5ab9d702f78f9eed9cef05c5d6df02", null ],
    [ "Models", "class_car_shop_1_1_data_1_1_database_entities.html#aa3aad0e4b02da21513a6509d6ed58bfe", null ]
];