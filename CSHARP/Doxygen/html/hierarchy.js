var hierarchy =
[
    [ "CarShop.Data.Brands", "class_car_shop_1_1_data_1_1_brands.html", null ],
    [ "DbContext", null, [
      [ "CarShop.Data.DatabaseEntities", "class_car_shop_1_1_data_1_1_database_entities.html", null ]
    ] ],
    [ "CarShop.Data.Extras", "class_car_shop_1_1_data_1_1_extras.html", null ],
    [ "IDisposable", null, [
      [ "CarShop.Logic.BrandLogic", "class_car_shop_1_1_logic_1_1_brand_logic.html", null ],
      [ "CarShop.Logic.ExtraConnectLogic", "class_car_shop_1_1_logic_1_1_extra_connect_logic.html", null ],
      [ "CarShop.Logic.ExtrasLogic", "class_car_shop_1_1_logic_1_1_extras_logic.html", null ],
      [ "CarShop.Logic.ModelLogic", "class_car_shop_1_1_logic_1_1_model_logic.html", null ],
      [ "CarShop.Logic.Test.BrandTest", "class_car_shop_1_1_logic_1_1_test_1_1_brand_test.html", null ],
      [ "CarShop.Logic.Test.ExtraConnectTest", "class_car_shop_1_1_logic_1_1_test_1_1_extra_connect_test.html", null ],
      [ "CarShop.Logic.Test.ExtraTest", "class_car_shop_1_1_logic_1_1_test_1_1_extra_test.html", null ],
      [ "CarShop.Logic.Test.ModelTest", "class_car_shop_1_1_logic_1_1_test_1_1_model_test.html", null ],
      [ "CarShop.Repository.BrandRepository", "class_car_shop_1_1_repository_1_1_brand_repository.html", null ],
      [ "CarShop.Repository.ExtraConnectRepository", "class_car_shop_1_1_repository_1_1_extra_connect_repository.html", null ],
      [ "CarShop.Repository.ExtrasRepository", "class_car_shop_1_1_repository_1_1_extras_repository.html", null ],
      [ "CarShop.Repository.ModelRepository", "class_car_shop_1_1_repository_1_1_model_repository.html", null ]
    ] ],
    [ "CarShop.Logic.ILogic< T >", "interface_car_shop_1_1_logic_1_1_i_logic.html", null ],
    [ "CarShop.Logic.ILogic< Brands >", "interface_car_shop_1_1_logic_1_1_i_logic.html", [
      [ "CarShop.Logic.BrandLogic", "class_car_shop_1_1_logic_1_1_brand_logic.html", null ]
    ] ],
    [ "CarShop.Logic.ILogic< Extras >", "interface_car_shop_1_1_logic_1_1_i_logic.html", [
      [ "CarShop.Logic.ExtrasLogic", "class_car_shop_1_1_logic_1_1_extras_logic.html", null ]
    ] ],
    [ "CarShop.Logic.ILogic< modelExtraConnect >", "interface_car_shop_1_1_logic_1_1_i_logic.html", [
      [ "CarShop.Logic.ExtraConnectLogic", "class_car_shop_1_1_logic_1_1_extra_connect_logic.html", null ]
    ] ],
    [ "CarShop.Logic.ILogic< Models >", "interface_car_shop_1_1_logic_1_1_i_logic.html", [
      [ "CarShop.Logic.ModelLogic", "class_car_shop_1_1_logic_1_1_model_logic.html", null ]
    ] ],
    [ "CarShop.Repository.IRepository< T >", "interface_car_shop_1_1_repository_1_1_i_repository.html", null ],
    [ "CarShop.Repository.IRepository< Brands >", "interface_car_shop_1_1_repository_1_1_i_repository.html", [
      [ "CarShop.Repository.BrandRepository", "class_car_shop_1_1_repository_1_1_brand_repository.html", null ]
    ] ],
    [ "CarShop.Repository.IRepository< CarShop.Data.Brands >", "interface_car_shop_1_1_repository_1_1_i_repository.html", null ],
    [ "CarShop.Repository.IRepository< CarShop.Data.Extras >", "interface_car_shop_1_1_repository_1_1_i_repository.html", null ],
    [ "CarShop.Repository.IRepository< CarShop.Data.modelExtraConnect >", "interface_car_shop_1_1_repository_1_1_i_repository.html", null ],
    [ "CarShop.Repository.IRepository< CarShop.Data.Models >", "interface_car_shop_1_1_repository_1_1_i_repository.html", null ],
    [ "CarShop.Repository.IRepository< Extras >", "interface_car_shop_1_1_repository_1_1_i_repository.html", [
      [ "CarShop.Repository.ExtrasRepository", "class_car_shop_1_1_repository_1_1_extras_repository.html", null ]
    ] ],
    [ "CarShop.Repository.IRepository< modelExtraConnect >", "interface_car_shop_1_1_repository_1_1_i_repository.html", [
      [ "CarShop.Repository.ExtraConnectRepository", "class_car_shop_1_1_repository_1_1_extra_connect_repository.html", null ]
    ] ],
    [ "CarShop.Repository.IRepository< Models >", "interface_car_shop_1_1_repository_1_1_i_repository.html", [
      [ "CarShop.Repository.ModelRepository", "class_car_shop_1_1_repository_1_1_model_repository.html", null ]
    ] ],
    [ "CarShop.Data.modelExtraConnect", "class_car_shop_1_1_data_1_1model_extra_connect.html", null ],
    [ "CarShop.Data.Models", "class_car_shop_1_1_data_1_1_models.html", null ]
];