var class_car_shop_1_1_data_1_1_models =
[
    [ "Models", "class_car_shop_1_1_data_1_1_models.html#ad4afc182b2a9fa227ed6a6f371573cb8", null ],
    [ "brand_id", "class_car_shop_1_1_data_1_1_models.html#a4d299f20f8a74077858b0f9542f77250", null ],
    [ "Brands", "class_car_shop_1_1_data_1_1_models.html#a58ffbf928f2b747f9aa9654a48ae3af9", null ],
    [ "engine_volume", "class_car_shop_1_1_data_1_1_models.html#afaa520783e841f47353dfdcc6fc2454b", null ],
    [ "horsepower", "class_car_shop_1_1_data_1_1_models.html#a2fe1c9ab79ce9e7dc64f797d4cd1b552", null ],
    [ "Id", "class_car_shop_1_1_data_1_1_models.html#afa710a932334395e921270955e4f7094", null ],
    [ "modelExtraConnect", "class_car_shop_1_1_data_1_1_models.html#adf2b5ad887fc55d408a701962e3ac87d", null ],
    [ "name", "class_car_shop_1_1_data_1_1_models.html#a41541d669db34fe8ffbb5d14fd152fe3", null ],
    [ "price", "class_car_shop_1_1_data_1_1_models.html#ab1c252389bc75dd127525bd7c7e6a67a", null ],
    [ "releasedate", "class_car_shop_1_1_data_1_1_models.html#a353bb4be3f32c089adedda89b7888338", null ]
];