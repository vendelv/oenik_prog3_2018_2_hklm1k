var namespace_car_shop_1_1_data =
[
    [ "Brands", "class_car_shop_1_1_data_1_1_brands.html", "class_car_shop_1_1_data_1_1_brands" ],
    [ "DatabaseEntities", "class_car_shop_1_1_data_1_1_database_entities.html", "class_car_shop_1_1_data_1_1_database_entities" ],
    [ "Extras", "class_car_shop_1_1_data_1_1_extras.html", "class_car_shop_1_1_data_1_1_extras" ],
    [ "modelExtraConnect", "class_car_shop_1_1_data_1_1model_extra_connect.html", "class_car_shop_1_1_data_1_1model_extra_connect" ],
    [ "Models", "class_car_shop_1_1_data_1_1_models.html", "class_car_shop_1_1_data_1_1_models" ]
];