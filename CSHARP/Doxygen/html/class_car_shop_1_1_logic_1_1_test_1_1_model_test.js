var class_car_shop_1_1_logic_1_1_test_1_1_model_test =
[
    [ "CanAddModel", "class_car_shop_1_1_logic_1_1_test_1_1_model_test.html#aab692f8093104051135685825d0b0772", null ],
    [ "CanGetModel", "class_car_shop_1_1_logic_1_1_test_1_1_model_test.html#a8ed123d7f21396b63437d19830954460", null ],
    [ "CanRemoveModel", "class_car_shop_1_1_logic_1_1_test_1_1_model_test.html#a19f328d91db4c5b66fbca389ad9e53e6", null ],
    [ "CanUpdateModel", "class_car_shop_1_1_logic_1_1_test_1_1_model_test.html#a4501c24ad8891242092c33130ce0bccb", null ],
    [ "Dispose", "class_car_shop_1_1_logic_1_1_test_1_1_model_test.html#ad0110115c59c5e961bd3e106582f1edf", null ],
    [ "Dispose", "class_car_shop_1_1_logic_1_1_test_1_1_model_test.html#aedbf167bbda0f6367f491c48d5429c98", null ],
    [ "Setup", "class_car_shop_1_1_logic_1_1_test_1_1_model_test.html#a755443e8d5c2bf84c4abcd1c5fd18bfb", null ]
];