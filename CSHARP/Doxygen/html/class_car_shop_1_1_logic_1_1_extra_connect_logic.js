var class_car_shop_1_1_logic_1_1_extra_connect_logic =
[
    [ "ExtraConnectLogic", "class_car_shop_1_1_logic_1_1_extra_connect_logic.html#a074fb6e50efdf50d563ed5067b1b2095", null ],
    [ "Create", "class_car_shop_1_1_logic_1_1_extra_connect_logic.html#a5c8980e9325d432c021fb9d03dd2046e", null ],
    [ "CreateForm", "class_car_shop_1_1_logic_1_1_extra_connect_logic.html#aabe69642ea613a5eaee59d320fff1508", null ],
    [ "Delete", "class_car_shop_1_1_logic_1_1_extra_connect_logic.html#a67c65da51c783fa98a81c287b1cafa28", null ],
    [ "Dispose", "class_car_shop_1_1_logic_1_1_extra_connect_logic.html#ac2f0bd482eb5180dfd34ae11bd033ccf", null ],
    [ "Dispose", "class_car_shop_1_1_logic_1_1_extra_connect_logic.html#a2a0c526a7671abe1512991f8444db82c", null ],
    [ "GetById", "class_car_shop_1_1_logic_1_1_extra_connect_logic.html#a240dc349125884ac3c2408c4b4783c85", null ],
    [ "GetId", "class_car_shop_1_1_logic_1_1_extra_connect_logic.html#ab83399cd7e9002c45e22f5abb351498e", null ],
    [ "List", "class_car_shop_1_1_logic_1_1_extra_connect_logic.html#a6ebc0fca2e6477c82d08908d3ada98fd", null ],
    [ "Update", "class_car_shop_1_1_logic_1_1_extra_connect_logic.html#afbe5ce589dc174d31a49e52dbfedae32", null ]
];