var class_car_shop_1_1_logic_1_1_test_1_1_brand_test =
[
    [ "CanAddBrand", "class_car_shop_1_1_logic_1_1_test_1_1_brand_test.html#af581870aa0df5c3db9e1602edcc2be5b", null ],
    [ "CanGetBrand", "class_car_shop_1_1_logic_1_1_test_1_1_brand_test.html#a458540507a28397f43fc88e07a3ea872", null ],
    [ "CanRemoveBrand", "class_car_shop_1_1_logic_1_1_test_1_1_brand_test.html#a5775b5dfb035a7e3f194d3e5e35fc723", null ],
    [ "CanUpdateBrand", "class_car_shop_1_1_logic_1_1_test_1_1_brand_test.html#a6ab06b31dfafe60950a2157fca746c31", null ],
    [ "Dispose", "class_car_shop_1_1_logic_1_1_test_1_1_brand_test.html#aee523fef2d1600ac0f347fde3d751ac0", null ],
    [ "Dispose", "class_car_shop_1_1_logic_1_1_test_1_1_brand_test.html#ae5dc8bc3074f97d60c18f7172f33f156", null ],
    [ "Setup", "class_car_shop_1_1_logic_1_1_test_1_1_brand_test.html#ad1ebf0ac4882f4ed32d356a63e925869", null ]
];