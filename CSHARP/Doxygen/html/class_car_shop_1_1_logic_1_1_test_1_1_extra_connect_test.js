var class_car_shop_1_1_logic_1_1_test_1_1_extra_connect_test =
[
    [ "CanAddExtraToModel", "class_car_shop_1_1_logic_1_1_test_1_1_extra_connect_test.html#aec9d3fdb7521a14f021fd66f34a8c0dc", null ],
    [ "CanGetExtraToModel", "class_car_shop_1_1_logic_1_1_test_1_1_extra_connect_test.html#ab641da3bc64aede8dfaa8ab2342e5ed2", null ],
    [ "CanRemoveExtraToModel", "class_car_shop_1_1_logic_1_1_test_1_1_extra_connect_test.html#aef1920a09e39c0131ad75e1d853c7963", null ],
    [ "CanUpdateExtraToModel", "class_car_shop_1_1_logic_1_1_test_1_1_extra_connect_test.html#a656cc1d2e8bb30468bd885ac4dd68300", null ],
    [ "Dispose", "class_car_shop_1_1_logic_1_1_test_1_1_extra_connect_test.html#a851fc42494e6e503fb878c1b439207b2", null ],
    [ "Dispose", "class_car_shop_1_1_logic_1_1_test_1_1_extra_connect_test.html#a259c553dbac76421b9108bf015fa6056", null ],
    [ "Setup", "class_car_shop_1_1_logic_1_1_test_1_1_extra_connect_test.html#a401771a8c09b08f1415fdc6fc08a3cb2", null ]
];