var namespace_car_shop_1_1_repository =
[
    [ "BrandRepository", "class_car_shop_1_1_repository_1_1_brand_repository.html", "class_car_shop_1_1_repository_1_1_brand_repository" ],
    [ "ExtraConnectRepository", "class_car_shop_1_1_repository_1_1_extra_connect_repository.html", "class_car_shop_1_1_repository_1_1_extra_connect_repository" ],
    [ "ExtrasRepository", "class_car_shop_1_1_repository_1_1_extras_repository.html", "class_car_shop_1_1_repository_1_1_extras_repository" ],
    [ "IRepository", "interface_car_shop_1_1_repository_1_1_i_repository.html", "interface_car_shop_1_1_repository_1_1_i_repository" ],
    [ "ModelRepository", "class_car_shop_1_1_repository_1_1_model_repository.html", "class_car_shop_1_1_repository_1_1_model_repository" ]
];