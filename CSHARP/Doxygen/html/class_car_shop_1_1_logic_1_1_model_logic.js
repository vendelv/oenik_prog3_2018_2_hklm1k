var class_car_shop_1_1_logic_1_1_model_logic =
[
    [ "ModelLogic", "class_car_shop_1_1_logic_1_1_model_logic.html#a8f5b74f7b1728285e1825c3bf7d4e2a5", null ],
    [ "Create", "class_car_shop_1_1_logic_1_1_model_logic.html#a111c1a74e26b33a7dc351f7f04fc3b7f", null ],
    [ "CreateForm", "class_car_shop_1_1_logic_1_1_model_logic.html#a95fc2231dcb75c83e2d627a61f53ee92", null ],
    [ "Delete", "class_car_shop_1_1_logic_1_1_model_logic.html#a95bc058886fe2103aea4bc1b173bc186", null ],
    [ "Dispose", "class_car_shop_1_1_logic_1_1_model_logic.html#a069877c813892d792844750a8bcf2567", null ],
    [ "Dispose", "class_car_shop_1_1_logic_1_1_model_logic.html#ad7a1c33b7f8bbfe56ff5e5a093becb36", null ],
    [ "GetById", "class_car_shop_1_1_logic_1_1_model_logic.html#ad8b6edea574c4062662526867b42a3ef", null ],
    [ "GetId", "class_car_shop_1_1_logic_1_1_model_logic.html#ab4ccad861712318b03d38898a56f5a0f", null ],
    [ "List", "class_car_shop_1_1_logic_1_1_model_logic.html#ae04ad7516558e38d35fffa40535635ec", null ],
    [ "Update", "class_car_shop_1_1_logic_1_1_model_logic.html#a2c1df1fd46c9d65c93242d8fe0a42f93", null ]
];