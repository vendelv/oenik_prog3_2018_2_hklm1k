var searchData=
[
  ['ilogic',['ILogic',['../interface_car_shop_1_1_logic_1_1_i_logic.html',1,'CarShop::Logic']]],
  ['ilogic_3c_20brands_20_3e',['ILogic&lt; Brands &gt;',['../interface_car_shop_1_1_logic_1_1_i_logic.html',1,'CarShop::Logic']]],
  ['ilogic_3c_20extras_20_3e',['ILogic&lt; Extras &gt;',['../interface_car_shop_1_1_logic_1_1_i_logic.html',1,'CarShop::Logic']]],
  ['ilogic_3c_20modelextraconnect_20_3e',['ILogic&lt; modelExtraConnect &gt;',['../interface_car_shop_1_1_logic_1_1_i_logic.html',1,'CarShop::Logic']]],
  ['ilogic_3c_20models_20_3e',['ILogic&lt; Models &gt;',['../interface_car_shop_1_1_logic_1_1_i_logic.html',1,'CarShop::Logic']]],
  ['irepository',['IRepository',['../interface_car_shop_1_1_repository_1_1_i_repository.html',1,'CarShop::Repository']]],
  ['irepository_3c_20brands_20_3e',['IRepository&lt; Brands &gt;',['../interface_car_shop_1_1_repository_1_1_i_repository.html',1,'CarShop::Repository']]],
  ['irepository_3c_20carshop_3a_3adata_3a_3abrands_20_3e',['IRepository&lt; CarShop::Data::Brands &gt;',['../interface_car_shop_1_1_repository_1_1_i_repository.html',1,'CarShop::Repository']]],
  ['irepository_3c_20carshop_3a_3adata_3a_3aextras_20_3e',['IRepository&lt; CarShop::Data::Extras &gt;',['../interface_car_shop_1_1_repository_1_1_i_repository.html',1,'CarShop::Repository']]],
  ['irepository_3c_20carshop_3a_3adata_3a_3amodelextraconnect_20_3e',['IRepository&lt; CarShop::Data::modelExtraConnect &gt;',['../interface_car_shop_1_1_repository_1_1_i_repository.html',1,'CarShop::Repository']]],
  ['irepository_3c_20carshop_3a_3adata_3a_3amodels_20_3e',['IRepository&lt; CarShop::Data::Models &gt;',['../interface_car_shop_1_1_repository_1_1_i_repository.html',1,'CarShop::Repository']]],
  ['irepository_3c_20extras_20_3e',['IRepository&lt; Extras &gt;',['../interface_car_shop_1_1_repository_1_1_i_repository.html',1,'CarShop::Repository']]],
  ['irepository_3c_20modelextraconnect_20_3e',['IRepository&lt; modelExtraConnect &gt;',['../interface_car_shop_1_1_repository_1_1_i_repository.html',1,'CarShop::Repository']]],
  ['irepository_3c_20models_20_3e',['IRepository&lt; Models &gt;',['../interface_car_shop_1_1_repository_1_1_i_repository.html',1,'CarShop::Repository']]]
];
