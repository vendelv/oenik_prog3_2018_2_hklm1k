var namespace_car_shop_1_1_logic =
[
    [ "Test", "namespace_car_shop_1_1_logic_1_1_test.html", "namespace_car_shop_1_1_logic_1_1_test" ],
    [ "BrandLogic", "class_car_shop_1_1_logic_1_1_brand_logic.html", "class_car_shop_1_1_logic_1_1_brand_logic" ],
    [ "ExtraConnectLogic", "class_car_shop_1_1_logic_1_1_extra_connect_logic.html", "class_car_shop_1_1_logic_1_1_extra_connect_logic" ],
    [ "ExtrasLogic", "class_car_shop_1_1_logic_1_1_extras_logic.html", "class_car_shop_1_1_logic_1_1_extras_logic" ],
    [ "ILogic", "interface_car_shop_1_1_logic_1_1_i_logic.html", "interface_car_shop_1_1_logic_1_1_i_logic" ],
    [ "ModelLogic", "class_car_shop_1_1_logic_1_1_model_logic.html", "class_car_shop_1_1_logic_1_1_model_logic" ]
];