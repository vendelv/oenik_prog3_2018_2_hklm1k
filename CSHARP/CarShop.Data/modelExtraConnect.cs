//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
#pragma warning disable 1591 

namespace CarShop.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class modelExtraConnect
    {
        public int Id { get; set; }
        public int model_id { get; set; }
        public int extra_id { get; set; }
    
        public virtual Extras Extras { get; set; }
        public virtual Models Models { get; set; }
    }
}
#pragma warning restore 1591
