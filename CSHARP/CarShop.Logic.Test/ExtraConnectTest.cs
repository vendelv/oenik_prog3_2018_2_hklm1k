﻿// <copyright file="ExtraConnectTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CarShop.Data;
    using CarShop.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Business Logic test class
    /// </summary>
    [TestFixture]
    public class ExtraConnectTest : IDisposable
    {
        private Mock<IRepository<modelExtraConnect>> mockRepository;
        private ExtraConnectLogic logic;
        private List<modelExtraConnect> list;
        private List<Models> listModel;
        private List<Extras> listExtra;
        private modelExtraConnect entity;

        /// <summary>
        /// Setup
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.mockRepository = new Mock<IRepository<modelExtraConnect>>();
            this.logic = new ExtraConnectLogic(this.mockRepository.Object);
            this.listModel = new List<Models>()
            {
                new Models() { Id = 1, brand_id = 2, name = "Model-X", engine_volume = 1, horsepower = 300, price = 100000000 },
                new Models() { Id = 2, brand_id = 2, name = "Model-S", engine_volume = 1, horsepower = 200, price = 5000000 }
            };
            this.listExtra = new List<Extras>()
            {
                new Extras() { Id = 1, categoryname = "Secutiry", color = "black", name = "Test 1", price = 10000, reuseable = 1 },
                new Extras() { Id = 2, categoryname = "Secutiry", color = "red", name = "Test 2", price = 20000, reuseable = 0 },
                new Extras() { Id = 3, categoryname = "Outside", color = "white", name = "Test 3", price = 30000, reuseable = 0 }
            };
            this.entity = new modelExtraConnect() { Id = 1, extra_id = 1, model_id = 1, Models = this.listModel[0], Extras = this.listExtra[0] };
            this.list = new List<modelExtraConnect>() { this.entity };
            this.mockRepository.Setup(x => x.SelectAll).Returns(this.list.AsQueryable());
        }

        /// <summary>
        /// Check if Logic can add new ExtraConnect
        /// </summary>
        [Test]
        public void CanAddExtraToModel()
        {
            modelExtraConnect newentity = new modelExtraConnect() { Id = 2, extra_id = 1, model_id = 2, Models = this.listModel[0], Extras = this.listExtra[1] };
            this.mockRepository.Setup(x => x.Create(newentity)).Callback((modelExtraConnect e) => this.list.Add(e));
            this.logic.Create(newentity);
            this.mockRepository.Verify(x => x.Create(newentity));
            Assert.AreEqual(2, this.list.Count);
        }

        /// <summary>
        /// Check if can get back ExtraConnect
        /// </summary>
        [Test]
        public void CanGetExtraToModel()
        {
            Assert.AreEqual(this.logic.GetById(1), this.list[0]);
        }

        /// <summary>
        /// Check if Logic can delete a ExtraConnect
        /// </summary>
        [Test]
        public void CanRemoveExtraToModel()
        {
            this.mockRepository.Setup(x => x.Delete(1)).Callback((int id) => this.list.Remove(this.entity));
            this.logic.Delete(1);
            Assert.AreEqual(0, this.list.Count);
        }

        /// <summary>
        /// Check if Logic can update a ExtraConnect
        /// </summary>
        [Test]
        public void CanUpdateExtraToModel()
        {
            this.mockRepository.Setup(x => x.Update(this.entity, "model_id", "2")).Callback((modelExtraConnect e, string f, string v) =>
            {
                this.list.Remove(e);
                this.list.Add(new modelExtraConnect()
                {
                    Id = 1,
                    extra_id = 1,
                    model_id = 2,
                    Models = this.listModel[1],
                    Extras = this.listExtra[0]
                });
            });
            this.logic.Update(1, "model_id", "2");
            Assert.AreEqual(this.list[0].model_id, 2);
        }

        /// <summary>
        /// Dispose database
        /// Source: https://docs.microsoft.com/en-us/visualstudio/code-quality/ca1063-implement-idisposable-correctly?view=vs-2017
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// The bulk of the clean-up code is implemented in Dispose(bool)
        /// </summary>
        /// <param name="disposing">disposing</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (this.logic != null)
                {
                    this.logic.Dispose();
                    this.logic = null;
                }
            }
        }
    }
}
