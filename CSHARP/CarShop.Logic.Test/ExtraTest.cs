﻿// <copyright file="ExtraTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CarShop.Data;
    using CarShop.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Business Logic test class
    /// </summary>
    [TestFixture]
    public class ExtraTest : IDisposable
    {
        private Mock<IRepository<Extras>> mockRepository;
        private ExtrasLogic logic;
        private List<Extras> list;
        private Extras entity;

        /// <summary>
        /// Setup
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.mockRepository = new Mock<IRepository<Extras>>();
            this.logic = new ExtrasLogic(this.mockRepository.Object);
            this.entity = new Extras() { Id = 1, categoryname = "Secutiry", color = "black", name = "Test 1", price = 10000, reuseable = 1 };
            this.list = new List<Extras>() { this.entity };
            this.mockRepository.Setup(x => x.SelectAll).Returns(this.list.AsQueryable());
        }

        /// <summary>
        /// Check if Logic can add new Extra
        /// </summary>
        [Test]
        public void CanAddExtra()
        {
            Extras newentity = new Extras() { Id = 3, categoryname = "Outside", color = "white", name = "Test 3", price = 30000, reuseable = 0 };
            this.mockRepository.Setup(x => x.Create(newentity)).Callback((Extras e) => this.list.Add(e));
            this.logic.Create(newentity);
            this.mockRepository.Verify(x => x.Create(newentity));
            Assert.AreEqual(2, this.list.Count);
        }

        /// <summary>
        /// Check if can get back extra
        /// </summary>
        [Test]
        public void CanGetExtra()
        {
            Assert.AreEqual(this.logic.GetById(1), this.list[0]);
        }

        /// <summary>
        /// Check if Logic can delete a Extra
        /// </summary>
        [Test]
        public void CanRemoveExtra()
        {
            this.mockRepository.Setup(x => x.Delete(1)).Callback((int id) => this.list.Remove(this.entity));
            this.logic.Delete(1);
            Assert.AreEqual(0, this.list.Count);
        }

        /// <summary>
        /// Check if Logic can update a Extra
        /// </summary>
        [Test]
        public void CanUpdateExtra()
        {
            this.mockRepository.Setup(x => x.Update(this.entity, "name", "Test 1 Edited")).Callback((Extras e, string f, string v) =>
            {
                this.list.Remove(e);
                this.list.Add(new Extras()
                {
                    Id = 1,
                    categoryname = "Secutiry",
                    color = "black",
                    name = "Test 1 Edited",
                    price = 10000,
                    reuseable = 1
                });
            });
            this.logic.Update(1, "name", "Test 1 Edited");
            Assert.AreEqual("Test 1 Edited", this.list[0].name);
        }

        /// <summary>
        /// Dispose database
        /// Source: https://docs.microsoft.com/en-us/visualstudio/code-quality/ca1063-implement-idisposable-correctly?view=vs-2017
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// The bulk of the clean-up code is implemented in Dispose(bool)
        /// </summary>
        /// <param name="disposing">disposing</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (this.logic != null)
                {
                    this.logic.Dispose();
                    this.logic = null;
                }
            }
        }
    }
}
