﻿// <copyright file="ModelTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CarShop.Data;
    using CarShop.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Business Logic test class
    /// </summary>
    [TestFixture]
    public class ModelTest : IDisposable
    {
        private Mock<IRepository<Models>> mockRepository;
        private ModelLogic logic;
        private List<Models> list;
        private Models entity;

        /// <summary>
        /// Setup
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.mockRepository = new Mock<IRepository<Models>>();
            this.logic = new ModelLogic(this.mockRepository.Object);
            this.entity = new Models() { Id = 1, brand_id = 2, name = "Model-X", engine_volume = 1, horsepower = 300, price = 100000000, releasedate = new DateTime(2018, 12, 7) };
            this.list = new List<Models>() { this.entity };
            this.mockRepository.Setup(x => x.SelectAll).Returns(this.list.AsQueryable());
        }

        /// <summary>
        /// Check if Logic can add new model
        /// </summary>
        [Test]
        public void CanAddModel()
        {
            Models newentity = new Models() { Id = 2, brand_id = 2, name = "Model-S", engine_volume = 1, horsepower = 200, price = 5000000, releasedate = new DateTime(2017, 5, 23) };
            this.mockRepository.Setup(x => x.Create(newentity)).Callback((Models e) => this.list.Add(e));
            this.logic.Create(newentity);
            this.mockRepository.Verify(x => x.Create(newentity));
            Assert.AreEqual(2, this.list.Count);
        }

        /// <summary>
        /// Check if can get back model
        /// </summary>
        [Test]
        public void CanGetModel()
        {
            Assert.AreEqual(this.logic.GetById(1), this.list[0]);
        }

        /// <summary>
        /// Check if Logic can delete a model
        /// </summary>
        [Test]
        public void CanRemoveModel()
        {
            this.mockRepository.Setup(x => x.Delete(1)).Callback((int id) => this.list.Remove(this.entity));
            this.logic.Delete(1);
            Assert.AreEqual(0, this.list.Count);
        }

        /// <summary>
        /// Check if Logic can update a model
        /// </summary>
        [Test]
        public void CanUpdateModel()
        {
            this.mockRepository.Setup(x => x.Update(this.entity, "price", "1000000")).Callback((Models e, string f, string v) =>
            {
                this.list.Remove(e);
                this.list.Add(new Models()
                {
                    Id = 1,
                    brand_id = 2,
                    name = "Model-X",
                    engine_volume = 1,
                    horsepower = 300,
                    price = 1000000,
                    releasedate = new DateTime(2018, 12, 7)
                });
            });
            this.logic.Update(1, "price", "1000000");
            Assert.AreEqual(1000000, this.list[0].price);
        }

        /// <summary>
        /// Dispose database
        /// Source: https://docs.microsoft.com/en-us/visualstudio/code-quality/ca1063-implement-idisposable-correctly?view=vs-2017
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// The bulk of the clean-up code is implemented in Dispose(bool)
        /// </summary>
        /// <param name="disposing">disposing</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (this.logic != null)
                {
                    this.logic.Dispose();
                    this.logic = null;
                }
            }
        }
    }
}
