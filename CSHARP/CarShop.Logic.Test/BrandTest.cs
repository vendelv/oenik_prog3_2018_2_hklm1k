﻿// <copyright file="BrandTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using CarShop.Data;
    using CarShop.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Business Logic test class
    /// </summary>
    [TestFixture]
    public class BrandTest : IDisposable
    {
        private Mock<IRepository<Brands>> mockRepository;
        private BrandLogic logic;
        private List<Brands> list;
        private Brands entity;

        /// <summary>
        /// Setup
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.mockRepository = new Mock<IRepository<Brands>>();
            this.logic = new BrandLogic(this.mockRepository.Object);
            this.entity = new Brands() { Id = 1, name = "BMW", country = "Germany", founded = 1800, income = 350, url = "www.bmw.com" };
            this.list = new List<Brands>() { this.entity };
            this.mockRepository.Setup(x => x.SelectAll).Returns(this.list.AsQueryable());
        }

        /// <summary>
        /// Check if Logic can add new Brand
        /// </summary>
        [Test]
        public void CanAddBrand()
        {
            Brands newentity = new Brands() { Id = 2, name = "Tesla", country = "USA", founded = 2007, income = 100, url = "www.tesla.com" };
            this.mockRepository.Setup(x => x.Create(newentity)).Callback((Brands e) => this.list.Add(e));
            this.logic.Create(newentity);
            this.mockRepository.Verify(x => x.Create(newentity));
            Assert.AreEqual(2, this.list.Count);
        }

        /// <summary>
        /// Check if can get back brand
        /// </summary>
        [Test]
        public void CanGetBrand()
        {
            Assert.AreEqual(this.logic.GetById(1), this.list[0]);
        }

        /// <summary>
        /// Check if Logic can delete a Brand
        /// </summary>
        [Test]
        public void CanRemoveBrand()
        {
            this.mockRepository.Setup(x => x.Delete(1)).Callback((int id) => this.list.Remove(this.entity));
            this.logic.Delete(1);
            Assert.AreEqual(0, this.list.Count);
        }

        /// <summary>
        /// Check if Logic can update a Brand
        /// </summary>
        [Test]
        public void CanUpdateBrand()
        {
            this.mockRepository.Setup(x => x.Update(this.entity, "income", "100")).Callback((Brands e, string f, string v) =>
            {
                this.list.Remove(e);
                this.list.Add(new Brands()
                {
                    Id = 1,
                    name = "BMW",
                    country = "Germany",
                    founded = 1800,
                    income = 100,
                    url = "www.bmw.com"
                });
            });
            this.logic.Update(1, "income", "100");
            Assert.AreEqual(100, this.list[0].income);
        }

        /// <summary>
        /// Dispose database
        /// Source: https://docs.microsoft.com/en-us/visualstudio/code-quality/ca1063-implement-idisposable-correctly?view=vs-2017
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// The bulk of the clean-up code is implemented in Dispose(bool)
        /// </summary>
        /// <param name="disposing">disposing</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (this.logic != null)
                {
                    this.logic.Dispose();
                    this.logic = null;
                }
            }
        }
    }
}
