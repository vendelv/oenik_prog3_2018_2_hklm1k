﻿// <copyright file="ModelLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic
{
    using System;
    using System.Linq;
    using CarShop.Data;
    using CarShop.Repository;

    /// <summary>
    /// CRUD operations of Brands
    /// </summary>
    public class ModelLogic : ILogic<Models>, IDisposable
    {
        private IRepository<Models> repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModelLogic"/> class.
        /// </summary>
        /// <param name="repo">Repository for testes (not required)!</param>
        public ModelLogic(IRepository<Models> repo = null)
        {
            if (repo == null)
            {
                this.repo = new ModelRepository();
            }
            else
            {
                this.repo = repo;
            }
        }

        /// <summary>
        /// Create new Brand in the database
        /// </summary>
        /// <param name="id">Brand unique ID</param>
        public void CreateForm(int id)
        {
            Models br = new Models();
            Console.WriteLine("Model name: ");
            br.name = Console.ReadLine();
            while (br.releasedate == null)
            {
                Console.WriteLine("Model release date: ");
                if (DateTime.TryParse(Console.ReadLine(), out DateTime f))
                {
                    br.releasedate = f;
                }
                else
                {
                    Console.WriteLine("Not well formated date!");
                }
            }

            while (br.engine_volume == 0)
            {
                Console.WriteLine("Engine volume: ");
                if (int.TryParse(Console.ReadLine(), out int f))
                {
                    br.engine_volume = f;
                }
                else
                {
                    Console.WriteLine("Only number is acceptable!");
                }
            }

            while (br.horsepower == 0)
            {
                Console.WriteLine("Horse power: ");
                if (int.TryParse(Console.ReadLine(), out int f))
                {
                    br.horsepower = f;
                }
                else
                {
                    Console.WriteLine("Only number is acceptable!");
                }
            }

            while (br.price == 0)
            {
                Console.WriteLine("Price: ");
                if (int.TryParse(Console.ReadLine(), out int f))
                {
                    br.price = f;
                }
                else
                {
                    Console.WriteLine("Only number is acceptable!");
                }
            }

            br.brand_id = id;
            br.releasedate = DateTime.Now;

            this.Create(br);
        }

        /// <summary>
        /// Create new model
        /// </summary>
        /// <param name="ins">Model instance</param>
        /// <returns>Returns true if the add was successful</returns>
        public bool Create(Models ins)
        {
            if (this.repo.Create(ins))
            {
                Console.WriteLine($"{ins.name} created!");
                return true;
            }
            else
            {
                Console.WriteLine($"{ins.name} creation was not successful!");
                return false;
            }
        }

        /// <summary>
        /// Delete a Model from the database by unique id
        /// </summary>
        /// <param name="id">Unique ID you want to delete</param>
        /// <returns>Returns true if the delete was successful</returns>
        public bool Delete(int id)
        {
            if (this.repo.Delete(id))
            {
                Console.WriteLine($"#{id} deleted!");
                return true;
            }
            else
            {
                Console.WriteLine($"{id} update was not successful!");
                return false;
            }
        }

        /// <summary>
        /// List all Model from the database
        /// </summary>
        /// <param name="id">Model id. If id = 0 it'll list all extra</param>
        public void List(int id = 0)
        {
            Console.WriteLine("#id\tBrand\tName\t\tRelease\t\tPrice\tExtras");
            foreach (var row in id == 0 ? this.repo.SelectAll : this.repo.SelectAll.Where(x => x.brand_id == id))
            {
                Console.WriteLine($"#{row.Id}\t{row.Brands.name}\t{row.name}{(row.name.Length > 7 ? "\t" : "\t\t")}{row.releasedate.Year}.{row.releasedate.Month}.{row.releasedate.Day}\t{row.price}Ft\t+{row.modelExtraConnect.Sum(x => x.Extras.price)}Ft");
            }
        }

        /// <summary>
        /// Update a Model in the database by id
        /// </summary>
        /// <param name="id">Unique brand ID you want to delete</param>
        /// <param name="field">Updateing field</param>
        /// <param name="value">Field new value</param>
        /// <returns>Returns true if the update was successful</returns>
        public bool Update(int id, string field, string value)
        {
            Models ud = this.repo.SelectAll.Where(x => x.Id == id).SingleOrDefault();
            if (this.repo.Update(ud, field, value))
            {
                Console.WriteLine($"#{ud.Id} {ud.name} updated!");
                return true;
            }
            else
            {
                Console.WriteLine($"#{ud.Id} {ud.name} creation was not successful!");
                return false;
            }
        }

        /// <summary>
        /// Search a Brand
        /// </summary>
        /// <param name="field">Updateing field</param>
        /// <param name="value">Field new value</param>
        /// <returns>Returns a Instance unique Id</returns>
        public IQueryable<int> GetId(string field, string value)
        {
            // I know this is not a good solution but I always got some LINQ error when I'm trying to parse the value to int.
            switch (field.ToLower())
            {
                case "id":
                    return this.repo.SelectAll.Where(x => x.Id.ToString() == value).Select(x => x.Id);
                case "brand_id":
                    return this.repo.SelectAll.Where(x => x.brand_id.ToString() == value).Select(x => x.Id);
                case "name":
                    return this.repo.SelectAll.Where(x => x.name == value).Select(x => x.Id);
                case "releasedate":
                    return this.repo.SelectAll.Where(x => x.releasedate == DateTime.Parse(value)).Select(x => x.Id);
                case "engine_volume":
                    return this.repo.SelectAll.Where(x => x.engine_volume.ToString() == value).Select(x => x.Id);
                case "horsepower":
                    return this.repo.SelectAll.Where(x => x.horsepower.ToString() == value).Select(x => x.Id);
                case "price":
                    return this.repo.SelectAll.Where(x => x.price.ToString() == value).Select(x => x.Id);
                default:
                    throw new ArgumentException("Invalid field!");
            }
        }

        /// <summary>
        /// Search a instances by Id
        /// </summary>
        /// <param name="id">Unique ID</param>
        /// <returns>Returns a Model instance</returns>
        public Models GetById(int id)
        {
            return (from x in this.repo.SelectAll
                    where x.Id == id
                    select x).SingleOrDefault();
        }

        /// <summary>
        /// Dispose database
        /// Source: https://docs.microsoft.com/en-us/visualstudio/code-quality/ca1063-implement-idisposable-correctly?view=vs-2017
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// The bulk of the clean-up code is implemented in Dispose(bool)
        /// </summary>
        /// <param name="disposing">disposing</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (this.repo != null)
                {
                    this.repo.Dispose();
                    this.repo = null;
                }
            }
        }
    }
}
