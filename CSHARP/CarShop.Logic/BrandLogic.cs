﻿// <copyright file="BrandLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CarShop.Data;
    using CarShop.Repository;

    /// <summary>
    /// CRUD operations of Brands
    /// </summary>
    public class BrandLogic : ILogic<Brands>, IDisposable
    {
        private IRepository<Brands> repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="BrandLogic"/> class.
        /// </summary>
        /// <param name="repo">Repository for testes (not required)!</param>
        public BrandLogic(IRepository<Brands> repo = null)
        {
            if (repo == null)
            {
                this.repo = new BrandRepository();
            }
            else
            {
                this.repo = repo;
            }
        }

        /// <summary>
        /// Form for createing new Brand
        /// </summary>
        public void CreateForm()
        {
            Brands br = new Brands();
            Console.WriteLine("Brand name: ");
            br.name = Console.ReadLine();
            Console.WriteLine("Brand country: ");
            br.country = Console.ReadLine();
            while (br.founded == 0)
            {
                Console.WriteLine("Brand founded: ");
                if (int.TryParse(Console.ReadLine(), out int f))
                {
                    br.founded = f;
                }
                else
                {
                    Console.WriteLine("Only number is acceptable!");
                }
            }

            while (br.income == 0)
            {
                Console.WriteLine("Brand income: ");
                if (int.TryParse(Console.ReadLine(), out int f))
                {
                    br.income = f;
                }
                else
                {
                    Console.WriteLine("Only number is acceptable!");
                }
            }

            Console.WriteLine("Brand url: ");
            br.url = Console.ReadLine();
            this.Create(br);
        }

        /// <summary>
        /// Create new brand
        /// </summary>
        /// <param name="ins">Brand instance</param>
        /// <returns>Returns true if the add was successful</returns>
        public bool Create(Brands ins)
        {
            if (this.repo.Create(ins))
            {
                Console.WriteLine($"{ins.name} created!");
                return true;
            }
            else
            {
                Console.WriteLine($"{ins.name} creation was not successful!");
                return false;
            }
        }

        /// <summary>
        /// Delete a Brand from the database by unique id
        /// </summary>
        /// <param name="id">Unique ID you want to delete</param>
        /// <returns>Returns true if the delete was successful</returns>
        public bool Delete(int id)
        {
            if (this.repo.Delete(id))
            {
                Console.WriteLine($"#{id} deleted!");
                return true;
            }
            else
            {
                Console.WriteLine($"{id} creation was not successful!");
                return false;
            }
        }

        /// <summary>
        /// List all Brand from the database
        /// </summary>
        public void List()
        {
            Console.WriteLine("#id\tName\tCountry\t\tFounded\tIncome\tModels\tURL");
            foreach (var row in this.repo.SelectAll)
            {
                Console.WriteLine($"#{row.Id}\t{row.name}\t{row.country}{(row.country.Length > 8 ? "\t" : "\t\t")}{row.founded}\t{row.income}M\t{row.Models.Count} db\t{row.url}");
            }
        }

        /// <summary>
        /// Update a Brand in the database by id
        /// </summary>
        /// <param name="id">Unique brand ID you want to delete</param>
        /// <param name="field">Updateing field</param>
        /// <param name="value">Field new value</param>
        /// <returns>Returns true if the update was successful</returns>
        public bool Update(int id, string field, string value)
        {
            Brands ud = this.repo.SelectAll.Where(x => x.Id == id).SingleOrDefault();
            if (this.repo.Update(ud, field, value))
            {
                Console.WriteLine($"#{ud.Id} {ud.name} updated!");
                return true;
            }
            else
            {
                Console.WriteLine($"#{ud.Id} {ud.name} update was not successful!");
                return false;
            }
        }

        /// <summary>
        /// Search a Brand
        /// </summary>
        /// <param name="field">Updateing field</param>
        /// <param name="value">Field new value</param>
        /// <returns>Returns a Instance unique Id</returns>
        public IQueryable<int> GetId(string field, string value)
        {
            // I know this is not a good solution but I always got some LINQ error when I'm trying to parse the value to int.
            switch (field.ToLower())
            {
                case "id":
                    return this.repo.SelectAll.Where(x => x.Id.ToString() == value).Select(x => x.Id);
                case "name":
                    return this.repo.SelectAll.Where(x => x.name == value).Select(x => x.Id);
                case "country":
                    return this.repo.SelectAll.Where(x => x.country == value).Select(x => x.Id);
                case "url":
                    return this.repo.SelectAll.Where(x => x.url == value).Select(x => x.Id);
                case "founded":
                    return this.repo.SelectAll.Where(x => x.founded.ToString() == value).Select(x => x.Id);
                case "income":
                    return this.repo.SelectAll.Where(x => x.income.ToString() == value).Select(x => x.Id);
                default:
                    throw new ArgumentException("Invalid field!");
            }
        }

        /// <summary>
        /// Search a instances by Id
        /// </summary>
        /// <param name="id">Unique ID</param>
        /// <returns>Returns a Brands instance</returns>
        public Brands GetById(int id)
        {
            return (from x in this.repo.SelectAll
                   where x.Id == id
                   select x).SingleOrDefault();
        }

        /// <summary>
        /// Get Brand Models
        /// </summary>
        /// <param name="id">Brand id</param>
        /// <returns>Returns a Brands Models</returns>
        public IQueryable<Models> GetModels(int id)
        {
            return from x in this.repo.SelectAll
                   where x.Id == id
                   select (Models)x.Models;
        }

        /// <summary>
        /// Dispose database
        /// Source: https://docs.microsoft.com/en-us/visualstudio/code-quality/ca1063-implement-idisposable-correctly?view=vs-2017
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// The bulk of the clean-up code is implemented in Dispose(bool)
        /// </summary>
        /// <param name="disposing">disposing</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (this.repo != null)
                {
                    this.repo.Dispose();
                    this.repo = null;
                }
            }
        }
    }
}
