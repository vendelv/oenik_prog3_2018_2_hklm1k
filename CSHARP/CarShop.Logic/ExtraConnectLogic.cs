﻿// <copyright file="ExtraConnectLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic
{
    using System;
    using System.Linq;
    using CarShop.Data;
    using CarShop.Repository;

    /// <summary>
    /// CRUD operations of Brands
    /// </summary>
    public class ExtraConnectLogic : ILogic<modelExtraConnect>, IDisposable
    {
        private IRepository<modelExtraConnect> repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtraConnectLogic"/> class.
        /// </summary>
        /// <param name="repo">Repository for testes (not required)!</param>
        public ExtraConnectLogic(IRepository<modelExtraConnect> repo = null)
        {
            if (repo == null)
            {
                this.repo = new ExtraConnectRepository();
            }
            else
            {
                this.repo = repo;
            }
        }

        /// <summary>
        /// Create new ExtraConnect in the database
        /// </summary>
        /// <param name="id">Not used!</param>
        public void CreateForm(int id = 0)
        {
            modelExtraConnect br = new modelExtraConnect();
            if (id == 0)
            {
                while (br.model_id == 0)
                {
                    Console.WriteLine("Model ID: ");
                    if (int.TryParse(Console.ReadLine(), out int f))
                    {
                        br.model_id = f;
                    }
                    else
                    {
                        Console.WriteLine("Only number is acceptable!");
                    }
                }
            }
            else
            {
                br.model_id = id;
            }

            while (br.extra_id == 0)
            {
                Console.WriteLine("Extra ID: ");
                if (int.TryParse(Console.ReadLine(), out int f))
                {
                    br.extra_id = f;
                }
                else
                {
                    Console.WriteLine("Only number is acceptable!");
                }
            }

            this.repo.Create(br);
            Console.WriteLine($"Extra added!");
        }

        /// <summary>
        /// Create new ExtraConnect
        /// </summary>
        /// <param name="ins">modelExtraConnect instance</param>
        /// <returns>Returns true if the add was successful</returns>
        public bool Create(modelExtraConnect ins)
        {
            if (this.repo.Create(ins))
            {
                Console.WriteLine($"Extra connected!");
                return true;
            }
            else
            {
                Console.WriteLine($"The connection was not successful!");
                return false;
            }
        }

        /// <summary>
        /// Delete a ExtraConnect from the database by unique id
        /// </summary>
        /// <param name="id">Unique brand ID you want to delete</param>
        /// <returns>Returns true if the delete was successful</returns>
        public bool Delete(int id)
        {
            if (this.repo.Delete(id))
            {
                Console.WriteLine($"#{id} removed!");
                return true;
            }
            else
            {
                Console.WriteLine($"{id} creation was not successful!");
                return false;
            }
        }

        /// <summary>
        /// List all ExtraConnect in the database
        /// </summary>
        /// <param name="id">Model id. If id = 0 it'll list all extra</param>
        public void List(int id = 0)
        {
            foreach (var row in this.repo.SelectAll)
            {
                Console.WriteLine($"#{row.Models.Id} {row.Models.name}: {row.Extras.name} - {row.Extras.price}Ft!");
            }
        }

        /// <summary>
        /// Update new ExtraConnect in the database
        /// </summary>
        /// <param name="id">Unique brand ID you want to delete</param>
        /// <param name="field">Updateing field</param>
        /// <param name="value">Field new value</param>
        /// <returns>Returns true if the update was successful</returns>
        public bool Update(int id, string field, string value)
        {
            modelExtraConnect ud = this.repo.SelectAll.Where(x => x.Id == id).SingleOrDefault();
            if (this.repo.Update(ud, field, value))
            {
                Console.WriteLine($"Field updated!");
                return true;
            }
            else
            {
                Console.WriteLine($"Update was not successful!");
                return false;
            }
        }

        /// <summary>
        /// Search a Brand
        /// </summary>
        /// <param name="field">Updateing field</param>
        /// <param name="value">Field new value</param>
        /// <returns>Returns a Instance unique Id</returns>
        public IQueryable<int> GetId(string field, string value)
        {
            // I know this is not a good solution but I always got some LINQ error when I'm trying to parse the value to int.
            switch (field.ToLower())
            {
                case "id":
                    return this.repo.SelectAll.Where(x => x.Id.ToString() == value).Select(x => x.Id);
                case "model_id":
                    return this.repo.SelectAll.Where(x => x.model_id.ToString() == value).Select(x => x.Id);
                case "extra_id":
                    return this.repo.SelectAll.Where(x => x.extra_id.ToString() == value).Select(x => x.Id);
                default:
                    throw new ArgumentException("Invalid field!");
            }
        }

        /// <summary>
        /// Search a instances by Id
        /// </summary>
        /// <param name="id">Unique ID</param>
        /// <returns>Returns a modelExtraConnect instance</returns>
        public modelExtraConnect GetById(int id)
        {
            return (from x in this.repo.SelectAll
                    where x.Id == id
                    select x).SingleOrDefault();
        }

        /// <summary>
        /// Dispose database
        /// Source: https://docs.microsoft.com/en-us/visualstudio/code-quality/ca1063-implement-idisposable-correctly?view=vs-2017
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// The bulk of the clean-up code is implemented in Dispose(bool)
        /// </summary>
        /// <param name="disposing">disposing</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (this.repo != null)
                {
                    this.repo.Dispose();
                    this.repo = null;
                }
            }
        }
    }
}
