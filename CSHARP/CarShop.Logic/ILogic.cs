﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic
{
    using System.Linq;

    /// <summary>
    /// CRUD operation interface
    /// </summary>
    /// <typeparam name="T">T is the current carshop model</typeparam>
    public interface ILogic<T>
    {
        /// <summary>
        /// Create new instance in the database
        /// </summary>
        /// <param name="instance">Instance</param>
        /// <returns>Returns true if the Create was successful</returns>
        bool Create(T instance);

        /// <summary>
        /// Delete a instance
        /// </summary>
        /// <param name="id">Instance unique ID you want to delete</param>
        /// <returns>Returns true if the Delete was successful</returns>
        bool Delete(int id);

        /// <summary>
        /// Update a instance field by id
        /// </summary>
        /// <param name="id">Unique brand ID you want to delete</param>
        /// <param name="field">Updateing field</param>
        /// <param name="value">Field new value</param>
        /// <returns>Returns true if the Update was successful</returns>
        bool Update(int id, string field, string value);

        /// <summary>
        /// Search a instances
        /// </summary>
        /// <param name="field">Updateing field</param>
        /// <param name="value">Field new value</param>
        /// <returns>Returns a Instance unique Ids</returns>
        IQueryable<int> GetId(string field, string value);

        /// <summary>
        /// Search a instances by Id
        /// </summary>
        /// <param name="id">Unique ID</param>
        /// <returns>Returns a T instance</returns>
        T GetById(int id);
    }
}
