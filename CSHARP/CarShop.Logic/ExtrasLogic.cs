﻿// <copyright file="ExtrasLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic
{
    using System;
    using System.Linq;
    using CarShop.Data;
    using CarShop.Repository;

    /// <summary>
    /// CRUD operations of Extra
    /// </summary>
    public class ExtrasLogic : ILogic<Extras>, IDisposable
    {
        private IRepository<Extras> repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtrasLogic"/> class.
        /// </summary>
        /// <param name="repo">Repository for testes (not required)!</param>
        public ExtrasLogic(IRepository<Extras> repo = null)
        {
            if (repo == null)
            {
                this.repo = new ExtrasRepository();
            }
            else
            {
                this.repo = repo;
            }
        }

        /// <summary>
        /// Create new Extra in the database
        /// </summary>
        /// <param name="id">Unique Model ID</param>
        public void CreateForm(int id = 0)
        {
            Extras br = new Extras();
            Console.WriteLine("Extra name: ");
            br.name = Console.ReadLine();
            Console.WriteLine("Extra category: ");
            br.categoryname = Console.ReadLine();
            Console.WriteLine("Extra color: ");
            br.color = Console.ReadLine();
            while (br.price == 0)
            {
                Console.WriteLine("Price: ");
                if (int.TryParse(Console.ReadLine(), out int f))
                {
                    br.price = f;
                }
                else
                {
                    Console.WriteLine("Only number is acceptable!");
                }
            }

            Console.WriteLine("Reuseable?: ");
            string answ = Console.ReadLine();
            br.reuseable = byte.Parse(answ == "1" || answ == "true" ? "1" : "0");

            Console.WriteLine($"Do you want to add this new extra to the actual model? (yes/no): ");
            answ = Console.ReadLine();

            this.Create(br);

            if (answ == "yes")
            {
                modelExtraConnect br2 = new modelExtraConnect();
                br2.model_id = id;
                br2.extra_id = br.Id;
            }
        }

        /// <summary>
        /// Create new extra
        /// </summary>
        /// <param name="ins">Extra instance</param>
        /// <returns>Returns true if the add was successful</returns>
        public bool Create(Extras ins)
        {
            if (this.repo.Create(ins))
            {
                Console.WriteLine($"{ins.name} created!");
                return true;
            }
            else
            {
                Console.WriteLine($"{ins.name} creation was not successful!");
                return false;
            }
        }

        /// <summary>
        /// Delete a Extra from the database by unique id
        /// </summary>
        /// <param name="id">Unique brand ID you want to delete</param>
        /// <returns>Returns true if the delete was successful</returns>
        public bool Delete(int id)
        {
            if (this.repo.Delete(id))
            {
                Console.WriteLine($"#{id} deleted!");
                return true;
            }
            else
            {
                Console.WriteLine($"{id} creation was not successful!");
                return false;
            }
        }

        /// <summary>
        /// List all Extra in the database
        /// </summary>
        /// <param name="id">Model id. If id = 0 it'll list all extra</param>
        public void List(int id = 0)
        {
            Console.WriteLine("#id\tCategory\tName\tPrice\tColor\tReuseable?");
            foreach (Extras row in id == 0 ? this.GetModelExtras() : this.GetModelExtras(id))
            {
                Console.WriteLine($"#{row.Id}\t{row.categoryname}\t{row.name}\t{row.price}\t{row.color}\t{row.reuseable}");
            }
        }

        /// <summary>
        /// Update a Extra in the database
        /// </summary>
        /// <param name="id">Unique brand ID you want to delete</param>
        /// <param name="field">Updateing field</param>
        /// <param name="value">Field new value</param>
        /// <returns>Returns true if the update was successful</returns>
        public bool Update(int id, string field, string value)
        {
            Extras ud = this.repo.SelectAll.Where(x => x.Id == id).SingleOrDefault();
            if (this.repo.Update(ud, field, value))
            {
                Console.WriteLine($"#{ud.Id} {ud.name} updated!");
                return true;
            }
            else
            {
                Console.WriteLine($"#{ud.Id} {ud.name} update was not successful!");
                return false;
            }
        }

        /// <summary>
        /// Check if value exits in the selected field
        /// </summary>
        /// <param name="field">Searching field</param>
        /// <param name="value">Field value</param>
        /// <returns>Return true if exits false if isn't</returns>
        public IQueryable<int> GetId(string field, string value)
        {
            // I know this is not a good solution but I always got some LINQ error when I'm trying to parse the value to int.
            switch (field.ToLower())
            {
                case "id":
                    return this.repo.SelectAll.Where(x => x.Id.ToString() == value).Select(x => x.Id);
                case "categoryname":
                    return this.repo.SelectAll.Where(x => x.categoryname == value).Select(x => x.Id);
                case "name":
                    return this.repo.SelectAll.Where(x => x.name == value).Select(x => x.Id);
                case "price":
                    return this.repo.SelectAll.Where(x => x.price.ToString() == value).Select(x => x.Id);
                case "color":
                    return this.repo.SelectAll.Where(x => x.color == value).Select(x => x.Id);
                /*case "reuseable":
                    return this.repo.SelectAll().Where(x => x.reuseable == byte.Parse(value == "true" ? "1" : "0")).Select(x => x.Id);*/
                default:
                    throw new ArgumentException("Invalid field!");
            }
        }

        /// <summary>
        /// Search a instances by Id
        /// </summary>
        /// <param name="id">Unique ID</param>
        /// <returns>Returns a Extras instance</returns>
        public Extras GetById(int id)
        {
            return (from x in this.repo.SelectAll
                    where x.Id == id
                    select x).SingleOrDefault();
        }

        /// <summary>
        /// Get a list of all Extras in a specific model
        /// </summary>
        /// <param name="id">Model id</param>
        /// <returns>Returns a IQueryable extra list</returns>
        public IQueryable<Extras> GetModelExtras(int id)
        {
            return from x in this.repo.SelectAll
                   from y in x.modelExtraConnect
                   where y.model_id == id
                   select x;
        }

        /// <summary>
        /// Get a list of all Extras
        /// </summary>
        /// <returns>Returns a IQueryable extra list</returns>
        public IQueryable<Extras> GetModelExtras()
        {
            return from x in this.repo.SelectAll
                   select x;
        }

        /// <summary>
        /// Dispose database
        /// Source: https://docs.microsoft.com/en-us/visualstudio/code-quality/ca1063-implement-idisposable-correctly?view=vs-2017
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// The bulk of the clean-up code is implemented in Dispose(bool)
        /// </summary>
        /// <param name="disposing">disposing</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (this.repo != null)
                {
                    this.repo.Dispose();
                    this.repo = null;
                }
            }
        }
    }
}
